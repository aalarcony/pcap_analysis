# PCAP Analysis
This project is capable of:
- Capture traffic via SSH
- Analyse pcap

# Informations
- The output folder contains
    - pcap
    - ips
    - dfs

# Instructions
- Create the output folder
- Create .env which contains at least the following information:
```
    CAPTURE_TIME_SECONDS=20
    IPINFO_TOKEN=1a54338d61e6d3
    INTERFACE_NAME=ens3
    HOST_IP=57.128.108.250
    HOST_USER=ubuntu
    SSH_PORT=22
```