import streamlit as st
from classes.capture import Capture
from constants import (
    CAPTURE_TIME_SECONDS,
    HOST_IP,
    HOST_USER,
    SSH_PORT,
    INTERFACE_NAME,
)

st.set_page_config(layout="wide")
st.title("TCP Capture Dashboard")

# Add a button that launches a new capture when clicked
with st.container():
    st.subheader("Run a new Capture")
    col1, col2, col3 = st.columns(3)

    with col1:
        host_ip = st.text_input(label="Host IP", value=HOST_IP)
        host_user = st.text_input(label="Host User", value=HOST_USER)

    with col2:
        ssh_port = st.number_input(label="SSH Port", value=SSH_PORT)
        interface_name = st.text_input(label="Interface Name", value=INTERFACE_NAME)

    with col3:
        capture_time = st.number_input(
            label="Capture time (seconds)", value=CAPTURE_TIME_SECONDS, step=5
        )
        st.text("")
        st.text("")
        if st.button("Start Capture"):
            Capture.capture(
                host_user=host_user,
                host_ip=host_ip,
                ssh_port=ssh_port,
                interface_name=interface_name,
                capture_time_seconds=capture_time,
            )

with st.container():
    st.subheader("Load a Capture")
    col1, col2 = st.columns([5, 1])
    # Add a dropdown with all available captures
    with col1:
        all_capture_names = Capture.get_capture_names()
        sel_capture = st.selectbox("Select a capture", all_capture_names)
        if sel_capture is not None:
            capture = Capture(sel_capture)

    with col2:
        st.text("")
        st.text("")
        st.button("Wireshark", on_click=capture.open_on_wireshark)


# Add dataframes
with st.container():
    st.subheader("Display Dataframes")

    # Filter functionality
    filter_query = st.text_input(label="Filter query", value="")
    if len(filter_query) > 0:
        capture.apply_filter_query(filter_query)

    if sel_capture is not None:
        tab1, tab2, tab3 = st.tabs(["Raw Data", "Src-Dst-Protocol", "IPs"])

        with tab1:
            st.dataframe(capture.df_filtered)

        with tab2:
            st.dataframe(capture.df_src_dst_protocols)

        with tab3:
            st.dataframe(capture.df_count_ip)

# Inspect packets
st.subheader("Inspect Packets")
if sel_capture is not None:
    col1, col2 = st.columns([2, 1])
    with col1:
        sel_packet = st.selectbox(
            "Select a packet number", [i for i in range(len(capture.all_packets))]
        )
        if sel_packet is not None:
            packet = capture.all_packets[sel_packet]
            st.text(packet.get_show_str())
    with col2:
        st.text("")
        st.text("")
        if st.button("Wireshark Packet") and sel_packet is not None:
            capture.open_on_wireshark(packet_number=sel_packet)
