from pathlib import Path
from dotenv import load_dotenv
import os
import json

load_dotenv()

ABS_PATH = Path(__file__).parent.absolute()
CAPTURE_TIME_SECONDS = int(os.getenv("CAPTURE_TIME_SECONDS"))
IPINFO_TOKEN = os.getenv("IPINFO_TOKEN")
INTERFACE_NAME = os.getenv("INTERFACE_NAME")
HOST_IP = os.getenv("HOST_IP")
HOST_USER = os.getenv("HOST_USER")
SSH_PORT = int(os.getenv("SSH_PORT"))

IPS_PATH = ABS_PATH / "output" / "ips"
CAPTURES_PATH = ABS_PATH / "output" / "pcap"
DFS_PATH = ABS_PATH / "output" / "df"

# Load the list of named IPs
with open(ABS_PATH / "ip-names.json", "r") as f:
    NAMED_IPS = json.load(f)
