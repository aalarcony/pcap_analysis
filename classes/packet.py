from __future__ import annotations
import scapy.all
import scapy.packet
import scapy.utils
from pathlib import Path


class Packet:
    def __init__(self, packet: scapy.packet.Packet):
        self.scapy_packet = packet

    @staticmethod
    def parse_pcap(pcap_filedir: str | Path) -> list[Packet]:
        """This function parses a pcap file and returns a list of Packet objects.

        Args:
            pcap_filedir (str): Path to pcap file

        Returns:
            list[Packet]: List of Packet objects
        """
        all_packets = scapy.all.rdpcap(str(pcap_filedir))
        return [Packet(packet) for packet in all_packets]

    @property
    def time(self) -> scapy.utils.EDecimal:
        return self.scapy_packet.time

    def get_data(self, first_time: scapy.utils.EDecimal | None = None) -> dict:
        """This function parses a packet and returns a dictionary with the data."""

        # Get Ips from IP Layer
        ip_layer = self.scapy_packet.getlayer("IP") or self.scapy_packet.getlayer(
            "IPv6"
        )
        src_ip, dst_ip = None, None
        if ip_layer is not None:
            src_ip = ip_layer.getfieldval("src")
            dst_ip = ip_layer.getfieldval("dst")

        # Get Ethernet layer
        ethernet_layer = self.scapy_packet.getlayer("Ethernet")

        # Get TCP layer
        tcp_layer = self.scapy_packet.getlayer("TCP")
        src_port, dst_port = None, None
        if tcp_layer is not None:
            src_port = tcp_layer.sport
            dst_port = tcp_layer.dport

        layers_not_raw = [
            layer
            for layer in self.scapy_packet.layers()
            if layer is not scapy.packet.Raw
        ]
        last_layer = self.scapy_packet.getlayer(layers_not_raw[-1])

        remove_fields = ["load", "options"]
        signature = [
            f"{key} = {val}"
            for key, val in last_layer.fields.items()
            if key not in remove_fields
        ]

        if first_time is not None:
            time = float(self.time - first_time)
        else:
            time = float(self.time)

        return dict(
            time=time,
            src_ip=src_ip,
            src_port=src_port,
            dst_ip=dst_ip,
            dst_port=dst_port,
            ip_type=ethernet_layer.getfieldval("type"),
            protocol=last_layer.name,
            signature=signature,
        )

    def get_show_str(self) -> str:
        """Returns a string with the show command of the packet."""
        return self.scapy_packet.show(dump=True)
