from __future__ import annotations
from .ips import IPs
from .packet import Packet
from constants import (
    CAPTURE_TIME_SECONDS,
    HOST_IP,
    HOST_USER,
    SSH_PORT,
    INTERFACE_NAME,
    CAPTURES_PATH,
    DFS_PATH,
    NAMED_IPS,
    ABS_PATH,
)
import os
import pandas as pd
import datetime
from pathlib import Path
import numpy as np
import requests


class Capture:
    def __init__(
        self,
        pcap_filename: str,
    ):
        self.pcap_filedir = Path(CAPTURES_PATH / pcap_filename)
        self.df_filedir = DFS_PATH / f"{self.pcap_filedir.stem}.xlsx"
        # Load packets
        self.all_packets = Packet.parse_pcap(self.pcap_filedir)

        # Parse to create dataframes and write them to disk
        if not self.df_filedir.exists():
            self.parse()

        # Load dataframes
        all_dfs = pd.read_excel(self.df_filedir, sheet_name=None, index_col=0)
        self.df_raw_data = all_dfs["raw_data"]
        # Apply ip names
        for col in ["src_ip", "dst_ip"]:
            self.df_raw_data[col] = self.df_raw_data[col].replace(NAMED_IPS)
        self.df_ip_infos = all_dfs["ip_infos"]
        self.df_filtered = self.df_raw_data.copy()

    @staticmethod
    def capture(
        host_user: str | None = None,
        host_ip: str | None = None,
        ssh_port: int | None = None,
        interface_name: str | None = None,
        capture_time_seconds: int | None = None,
        filter_ssh: bool = True,
    ) -> Capture:
        """This function captures packets from a remote host and creates a new
        Capture object.

        Returns:
            Capture: Holds information for current capture
        """

        # Parse options
        host_user = host_user or HOST_USER
        host_ip = host_ip or HOST_IP
        ssh_port = ssh_port or SSH_PORT
        interface_name = interface_name or INTERFACE_NAME
        capture_time_seconds = capture_time_seconds or CAPTURE_TIME_SECONDS

        curr_time = datetime.datetime.now(datetime.UTC).strftime("%Y-%m-%d_%H:%M:%S")
        capture_file_path = (
            CAPTURES_PATH / f"{curr_time}-capture-{host_ip}-{interface_name}.pcap"
        )

        # Create folder if it doesn't exist
        capture_file_path.parent.mkdir(parents=True, exist_ok=True)

        # Download data
        print("Capturing packets...")
        filter_tcpdump = ""
        if filter_ssh:
            public_ip = Capture.get_public_ip()
            filter_tcpdump = f'"not (host {public_ip} and port 22)"'
        tcpdump_cmd = f"sudo tcpdump -U -i {interface_name} -G {capture_time_seconds} -w - {filter_tcpdump}"
        cmd = f"ssh {host_user}@{host_ip} -p {ssh_port} '{tcpdump_cmd}' > {capture_file_path}"
        print("Running:", cmd)
        os.system(cmd)

        return Capture(capture_file_path.name)

    def apply_filter_query(self, query: str):
        self.df_filtered = self.df_raw_data.query(query)

    def parse(self) -> pd.DataFrame:
        # Create folder if it doesn't exist
        self.df_filedir.parent.mkdir(parents=True, exist_ok=True)

        # Create a dataframe with the data
        first_time = self.all_packets[0].time
        data_packets = [
            packet.get_data(first_time=first_time) for packet in self.all_packets
        ]
        df = pd.DataFrame.from_records(data_packets)

        # Add ipinfo data
        unique_src = df["src_ip"].dropna().unique()
        unique_dst = df["dst_ip"].dropna().unique()
        all_ips = np.concatenate([unique_src, unique_dst])
        unique_ips = np.unique(all_ips)
        ips = IPs(unique_ips.tolist())

        # Save dataframes
        with pd.ExcelWriter(self.df_filedir) as writer:
            df.to_excel(writer, sheet_name="raw_data")
            ips.df_infos.to_excel(writer, sheet_name="ip_infos")

    def open_on_wireshark(self, packet_number: int | str | None = None):
        cmd_packet = ""
        if packet_number is not None:
            cmd_packet = f"-g {int(packet_number) + 1}"
        print("cmd_packet", cmd_packet)
        os.system(f"wireshark {cmd_packet} {self.pcap_filedir}")

    @staticmethod
    def get_public_ip():
        response = requests.get("https://api.ipify.org")
        if response.status_code == 200:
            return response.text
        else:
            return "Error: Unable to fetch IP"

    @property
    def df_src_dst_protocols(self) -> pd.DataFrame:
        """Returns a dataframe with the groupby src-dst-protocol.

        Returns:
            pd.DataFrame: Dataframe with groupby src-dst-protocol
        """
        return self.df_filtered.groupby(["src_ip", "dst_ip", "protocol"]).size()

    @property
    def df_count_ip(self) -> pd.DataFrame:
        """Returns a dataframe with the groupby ips.

        Returns:
            pd.DataFrame: Dataframe with groupby ips
        """
        df_src_ip = (
            self.df_filtered["src_ip"]
            .value_counts()
            .reset_index(name="count_src")
            .rename(columns={"src_ip": "ip"})
        )
        df_dst_ip = (
            self.df_filtered["dst_ip"]
            .value_counts()
            .reset_index(name="count_dst")
            .rename(columns={"dst_ip": "ip"})
        )
        df_count_ip = df_src_ip.merge(df_dst_ip, on="ip", how="outer").fillna(0)
        df_count_ip["total_count"] = df_count_ip["count_src"] + df_count_ip["count_src"]
        df_count_ip = df_count_ip.sort_values(by="total_count", ascending=False)
        df_count_ip = df_count_ip.merge(self.df_ip_infos, on="ip", how="left")

        return df_count_ip

    @staticmethod
    def get_capture_names() -> list[str]:
        """Returns a list of capture names in the CAPTURES_PATH folder.

        Returns:
            list[str]: List of capture names
        """
        return sorted([capture.name for capture in CAPTURES_PATH.glob("*.pcap")])
