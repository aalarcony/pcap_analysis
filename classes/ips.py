import asyncio
from aiohttp import ClientSession
from constants import IPINFO_TOKEN, IPS_PATH
import json
import pandas as pd


class IPs:
    def __init__(self, ips: list[str]):
        self.ip_data = {ip: None for ip in ips}
        self.__load_existing_ips()
        self.__fetch_lacking_ips()
        self.__generate_df()

    @property
    def lacking_ips(self) -> list[str]:
        # Returns ips for which data is lacking
        return [ip for ip, data in self.ip_data.items() if data is None]

    def __generate_df(self):
        self.df_infos = pd.DataFrame.from_records(list(self.ip_data.values()))

    def __load_existing_ips(self):
        # Loads existing information in IPS_PATH
        for ip in self.lacking_ips:
            filepath = IPS_PATH / f"{ip}.json"
            if filepath.exists():
                with open(filepath, "r") as f:
                    self.ip_data[ip] = json.load(f)

    def __fetch_lacking_ips(self):
        # Fetches information for lacking ips
        async def fetch(session: ClientSession, url: str) -> str:
            async with session.get(url) as response:
                return await response.json()

        async def main(urls):
            async with ClientSession() as session:
                tasks = [fetch(session, url) for url in urls]
                return await asyncio.gather(*tasks)

        # Set URLs
        urls = [
            f"https://ipinfo.io/{ip}?token={IPINFO_TOKEN}" for ip in self.lacking_ips
        ]

        responses = asyncio.run(main(urls))

        for ip, response in zip(self.lacking_ips, responses):
            # Save to ip_data
            self.ip_data[ip] = response
            # Save to disk
            with open(IPS_PATH / f"{ip}.json", "w") as f:
                json.dump(response, f, indent=1)
